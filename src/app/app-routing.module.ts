import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {
    path: 'login',
    redirectTo: 'auth',
    pathMatch:'full'
  },
  {
    path: '',
    loadChildren:()=>import('./layouts/user/user.module').then(m=>m.UserModule)
  },
  {
    path: 'auth',
    loadChildren:()=>import('./auth/auth.module').then(m=>m.AuthModule)
  },
  {
    path: 'home',
    loadChildren:()=>import('./layouts/user/user.module').then(m=>m.UserModule)
  },
  {
    path: 'dashboard',
    loadChildren:()=>import('./layouts/admin/admin.module').then(m=>m.AdminModule)
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

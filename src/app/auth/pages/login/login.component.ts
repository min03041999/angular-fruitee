import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formLogin: FormGroup;
  error = '';
  submitted = false;
  constructor() {
    this.formLogin = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    })
  }
  ngOnInit(): void {
  }

  submitLogin(){
    const regex = /^\S*$/;
    this.submitted = true;
    if (this.formLogin.invalid) {
      if (this.formLogin.controls.password.value == '' && this.formLogin.controls.email.value == '') {
        this.error = 'Vui lòng nhập email và password của bạn'
      } else {
        if (this.formLogin.controls.email.value == '') {
          this.error = 'Vui lòng nhập email của bạn'
        }
        if (!regex.test(this.formLogin.controls.email.value)) {
          this.error = 'Email người dùng không được chứa khoảng chắn'
        }
        if (this.formLogin.controls.password.value == '') {
          this.error = 'Vui lòng nhập password của bạn'
        }
      }
      this.submitted = false;
      return false;
    }
  }

}

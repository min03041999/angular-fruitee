import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public area: any[] = [
    {
      id: 1,
      name: 'Tp.Hồ Chí Minh',
      alias: 'HCM',
      status: true,
      created_at: '17/4/2021',
      updated_at: '20/4/2021'
    },
    {
      id: 2,
      name: 'Tp.Hà Nội',
      alias: 'HN',
      status: true,
      created_at: '13/3/2021',
      updated_at: '04/4/2021'
    },
    {
      id: 3,
      name: 'Phú Quốc',
      alias: 'PQ',
      status: false,
      created_at: '19/3/2021',
      updated_at: '08/4/2021'
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}

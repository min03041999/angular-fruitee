import { Component, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogDetailComponent } from './dialog-detail/dialog-detail.component';


@Component({
  selector: 'app-list-menu',
  templateUrl: './list-menu.component.html',
  styleUrls: ['./list-menu.component.scss']
})
export class ListMenuComponent implements OnInit {

  constructor(public dialog:MatDialog) { }

  products = [
    {
      id: 1,
      name: 'Frozen Orange',
      nametrans: 'Nước Cam Tươi',
      cate: 'Sinh tố',
      capacity: '400ml - 700ml',
      text: 'Nước cam tươi dùng 100% cam tươi kết hợp cùng lá bạc hà tuỳ lựa chọn của bạn. Vị ngọt thanh và giòn rụm của cam tươi kết hợp cùng vị cam thanh mát sẽ giúp bạn xua đi cơn khát giữa những ngày oi bức ở Sài Gòn.',
      utility: 'Nên dùng cam tươi, mức đường 80% và dùng kèm topping thạch chanh dây để tăng hương vị bạn nhé! Ghé ngay cửa hàng Fruitee để tậu một ly thôi nào!',
      img: 'https://i.pinimg.com/564x/8e/0e/ff/8e0effa24aacbc1eba4646b84a1529f1.jpg',
      price: 25000,
      price1: 30000
    },
    {
      id: 2,
      name: 'Taro Bubble Milk Tea',
      nametrans: 'Trà Sữa Khoai Môn',
      cate: 'Trà sữa',
      capacity: '400ml - 650ml',
      text: 'Trà sữa khoai môn tươi là thức uống biến tấu độc đáo từ công thức pha chế trà sữa truyền thống. Hai nguyên liệu chính tạo nên hương vị hấp dẫn và màu sắc đẹp mắt cho món thức uống này là khoai môn tươi béo bùi và bột khoai môn thơm ngát.',
      utility: 'Bột khoai môn làm trà sữa được chế biến từ nguyên liệu hoàn toàn tự nhiên. Nhờ vậy, hương vị thức uống đậm đà, béo ngon và thơm hơn so với sử dụng các loại syrup.',
      img: 'https://i.pinimg.com/564x/77/27/69/772769d1788d6907633d04abfa44ec0d.jpg',
      price: 25000,
      price1: null
    },
    {
      id: 3,
      name: 'Apple Iced Tea',
      nametrans: 'Trà Táo',
      cate: 'Trà trái cây',
      capacity: '400ml - 680ml',
      text: 'Trà táo đỏ hay trà táo tàu không chỉ là một loại trà thanh nhiệt mà còn rất có lợi cho sức khỏe con người. Uống trà táo đỏ thường xuyên sẽ giúp bổ sung khí huyết, lợi tim phổi, cải thiện giấc ngủ và làm đẹp da.',
      utility: 'Táo đỏ kết hợp với vị cay ấm của gừng mang lại tác dụng làm ấm cơ thể, an thần, giảm stress, bồi bổ cơ thể.',
      img: 'https://i.pinimg.com/564x/3a/81/3a/3a813a827e17ffa85a53d6baf7ca1cfc.jpg',
      price: 25000,
      price1: 32000
    },
    {
      id: 4,
      name: 'Honey Rosemary',
      nametrans: 'Nước Ép Honey Rosemary',
      cate: 'Nước ép',
      capacity: '400ml - 700ml',
      text: 'Nước ép Honey Rosemary giúp tăng cường trí nhớ, đào thải toxins, chống vi khuẩn đường ruột, phòng và hỗ trợ bệnh đau nửa đầu (Migraine), chống lão hóa.',
      utility: 'Những sản phẩm từ hương mật ong đã có mặt trên thị trường: kẹo quất mật ong Orion, kẹo gum hương bạc hà- mật ong.',
      img: 'https://i.pinimg.com/564x/39/cf/67/39cf67c4d75e5f8be8fe3372a7b6c572.jpg?fbclid=IwAR2mKwoSWvXDDwPTj_wnnLtSzZOXgYJ9IuZZBaOpxqLm7cMxgpWFyHOMZck',
      price: 28000,
      price1: null
    },
    {
      id: 5,
      name: 'Passion Fruit Iced Tea',
      nametrans: 'Nước Ép Chanh Dây',
      cate: 'Nước ép',
      capacity: '400ml - 700ml',
      text: 'Chanh dây hay còn gọi là chanh leo (trong tiếng anh gọi là passion fruit) là loại trái cây nhiệt đới rất giàu chất dinh dưỡng, dễ chế biến thành nước uống giúp thanh lọc cơ thể, tăng cường hệ miễn dịch và ngừa bệnh hô hấp.',
      utility: 'Loại nước ép này phổ biến trên khắp thế giới bởi hương vị của nó vô cùng đặc biệt và khẩu vị phù hợp với nhiều người.',
      img: 'https://i.pinimg.com/564x/8f/2e/a4/8f2ea45a8c50a3f517097a4bbd25b233.jpg',
      price: 30000,
      price1: 35000
    },
    {
      id: 6,
      name: 'Brown Sugar Milk Tea',
      nametrans: 'Trà Sữa Đường Nâu',
      cate: 'Trà sữa',
      capacity: '400ml - 700ml',
      text: 'Đường nâu – loại đường làm nên hương vị đặc biệt cho trà sữa đường nâu – Đường nâu tự nhiên: là đường mía vẫn còn lẫn rỉ (gỉ) mật, hay còn gọi là mật đường, chưa được tẩy trắng.',
      utility: 'Trà sữa đường nâu đích thực là loại đồ uống tuyệt vời dành cho người hảo ngọt đấy.',
      img: 'https://i.pinimg.com/564x/b0/47/91/b0479100c5dbcf479247f96908ce3a76.jpg',
      price: 30000,
      price1: 38000
    },
    {
      id: 7,
      name: 'Peach & Raspberry',
      nametrans: 'Sinh Tố Đào & Mâm Xôi',
      cate: 'Sinh tố',
      capacity: '400ml - 700ml',
      text: 'Sinh tố đào và mâm xôi là cải thiện sức khỏe đường ruột sẽ giúp hệ tiêu hóa hoạt động trơn tru và hiệu quả hơn. Các lợi khuẩn sống trong ruột không chỉ tăng cường miễn dịch mà còn hỗ trợ bạn tiêu hóa và hấp thụ nhiều chất dinh dưỡng hơn.',
      utility: 'Sinh tố đào, mâm xôi Về Công thức sinh tố đào, mâm xôi và quả hạch: Một bát sinh tố trông đẹp mắt và không kém phần dễ chịu đối với dạ dày và sức khỏe.',
      img: 'https://i.pinimg.com/564x/4a/22/ac/4a22ac2e2ed27737d5a89c194a581d70.jpg',
      price: 28000,
      price1: 35000
    },
    {
      id: 8,
      name: 'Orange Iced Tea',
      nametrans: 'Trà Cam',
      cate: 'Trà trái cây',
      capacity: '400ml - 700ml',
      text: 'Trà cam quế có vị chua nhẹ của cam tươi, một chút cay nóng từ quế, hương thơm dìu dịu, dù uống nóng hay dùng với đá cũng là một thức uống dễ chịu ...',
      utility: 'Trà cam sử dụng 100% trái cây tươi tự nhiên. Dùng cam giúp bạn lấy lại nguồn cảm hứng mới mỗi ngày, tăng sức đề kháng cho cơ thể và hỗ trợ làm đẹp da hiệu quả.',
      img: 'https://i.pinimg.com/564x/7a/98/ea/7a98ea53d7e13d83ff5ed3fe01c0235f.jpg',
      price: 28000,
      price1: 35000
    }
  ]


  categories = ['Signature', 'Trà trái cây', 'Trà sữa', 'Nước ép', 'Sinh tố'];
  selectedList:any;


  ngOnInit(){
    this.selectedList = this.categories[0];
  }

  openMenuList(category: any){
    console.log(category)
    this.selectedList = category;
  }

  showOn(res){
    const dialogRef = this.dialog.open(DialogDetailComponent, {
      width: '1000px',
      data: res
    });
  }
}

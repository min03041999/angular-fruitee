import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuRoutingModule } from './menu-routing.module';
import { MenuComponent } from './menu.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ListMenuComponent } from './components/list-menu/list-menu.component';
import { ToppingComponent } from './components/topping/topping.component';
import { DialogDetailComponent } from './components/list-menu/dialog-detail/dialog-detail.component';

import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    MenuComponent,
    CarouselComponent,
    ListMenuComponent,
    ToppingComponent,
    DialogDetailComponent
  ],
  imports: [
    CommonModule,
    MenuRoutingModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class MenuModule { }

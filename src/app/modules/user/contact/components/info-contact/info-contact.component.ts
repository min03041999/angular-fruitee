import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-contact',
  templateUrl: './info-contact.component.html',
  styleUrls: ['./info-contact.component.scss']
})
export class InfoContactComponent implements OnInit {

  selectedList:any;

  menuLists = [
    {
      name: 'Fruitee Sư Vạn Hạnh',
      adress: '828/38 Sư Vạn Hạnh, P14, Q10, TP.HCM',
      time: '8h - 22h | Thứ 2 - Chủ nhật'
    },
    {
      name: 'Fruitee Ba Tháng Hai',
      adress: '606/38 Ba Tháng Hai, P14 Q10, TP.HCM',
      time: '10h - 22h | Thứ 2 - Chủ nhật'
    },
    {
      name: 'Fruitee Hồ Con Rùa',
      adress: '828/38 Sư Vạn Hạnh, P14, Q10, TP.HCM',
      time: '8h - 22h | Thứ 2 - Chủ nhật'
    },
    {
      name: 'Fruitee Nguyễn Huệ',
      adress: '828/38 Sư Vạn Hạnh, P14, Q10, TP.HCM',
      time: '8h - 22h | Thứ 2 - Chủ nhật'
    },
  ]


  constructor() { }

  ngOnInit(){

    this.selectedList = this.menuLists[0].name;

  }



  openMenuList(menuList: any){
    console.log(menuList.name)
    this.selectedList = menuList.name;
  }


}

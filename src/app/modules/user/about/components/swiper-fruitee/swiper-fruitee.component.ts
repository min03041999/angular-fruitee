import { Component, OnInit } from '@angular/core';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-swiper-fruitee',
  templateUrl: './swiper-fruitee.component.html',
  styleUrls: ['./swiper-fruitee.component.scss']
})
export class SwiperFruiteeComponent {


  slideData = [
    {
      img: "../../../../../../assets/Images/swiper-5.png",
    }, {
      img: "../../../../../../assets/Images/swiper-6.png",
    }, {
      img: "../../../../../../assets/Images/swiper-7.png",
    }, {
      img: "../../../../../../assets/Images/swiper-8.png",
    }
  ]

  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    autoHeight: true,
    allowTouchMove: true,
    autoplay: {
      delay: 6000,
      disableOnInteraction: true
    },

    breakpoints: {
      1024: {
        slidesPerView: 1.5,
        spaceBetween: 30
      },
      500: {
        slidesPerView: 1.5,
        spaceBetween: 30
      },
      400: {
        slidesPerView: 1.5,
        spaceBetween: 30
      },
      300: {
        slidesPerView: 1.5,
        spaceBetween: 30
      }
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    loop: true
  };
}

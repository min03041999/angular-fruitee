import { Component, OnInit } from '@angular/core';
// import SwiperCore, {
//   Navigation,
//   Pagination,
//   Scrollbar,
//   A11y,
// } from 'swiper/core';

// SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-swiper-slide',
  templateUrl: './swiper-slide.component.html',
  styleUrls: ['./swiper-slide.component.scss'],
})
export class SwiperSlideComponent{
  // onSwiper(swiper) {
  //   console.log(swiper);
  // }
  // onSlideChange() {
  //   console.log('slide change');
  // }



  slideData = [
    {
      img: "../../../../../../assets/Images/main img.png",
    }, {
      img: "../../../../../../assets/Images/swiper-2.png",
    }, {
      img: "../../../../../../assets/Images/swiper-3.png",
    }, {
      img: "../../../../../../assets/Images/swiper-4.png",
    }
  ]

  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    autoHeight: true,
    allowTouchMove: true,
    autoplay: {
      delay: 6000,
      disableOnInteraction: true
    },

    breakpoints: {
      1024: {
        slidesPerView: 1.5,
        spaceBetween: 30
      },
      500: {
        slidesPerView: 1.5,
        spaceBetween: 30
      },
      400: {
        slidesPerView: 1.5,
        spaceBetween: 30
      },
      300: {
        slidesPerView: 1.5,
        spaceBetween: 30
      }
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    loop: true
  };
}

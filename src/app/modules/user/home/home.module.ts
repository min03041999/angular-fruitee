import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { SwiperSlideComponent } from './components/swiper-slide/swiper-slide.component';
import { SwiperAboutComponent } from './components/swiper-about/swiper-about.component';
import { DialogAboutComponent } from './components/swiper-about/dialog-about/dialog-about.component';

// import { SwiperModule } from 'swiper/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuComponent } from './components/menu/menu.component';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { MapsComponent } from './components/maps/maps.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    HomeComponent,
    CarouselComponent,
    SwiperSlideComponent,
    SwiperAboutComponent,
    MenuComponent,
    MapsComponent,
    DialogAboutComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    // SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    NgxUsefulSwiperModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule
  ],
  bootstrap: [HomeComponent]
})
export class HomeModule { }

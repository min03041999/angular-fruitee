import { Component, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogDetailComponent } from './dialog-detail/dialog-detail.component';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  news = [
    {
      id: 1,
      img: "../../../../../../assets/Images/news-1.png",
      name: 'BUY 1 MILKTEA - GET 1 FOR FREE',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: '05.01.2021 - 12.01.2021',
      map: 'Tất cả cửa hàng Fruitee',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    },
    {
      id: 2,
      img: "../../../../../../assets/Images/news-2.png",
      name: 'NEW DRINK: GREEN APPLE JUICE GIÁ CHỈ TỪ 20K',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: '01.01.2021 - 07.01.2021',
      map: 'Tất cả cửa hàng Fruitee',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    },
    {
      id: 3,
      img: "../../../../../../assets/Images/news-3.png",
      name: 'UỐNG MILKTEA - NHỚ ĐẾN FRUITEE MỖI THỨ 6',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: 'Không giới hạn',
      map: 'Chỉ áp dụng tại Fruitee Sư Vạn Hạnh',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    },
    {
      id: 4,
      img: "../../../../../../assets/Images/news-4.png",
      name: 'MIỄN PHÍ UP SIZE MỖI CHỦ NHẬT HÀNG TUẦN',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: 'Không giới hạn',
      map: 'Tất cả cửa hàng Fruitee',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    },
    {
      id: 5,
      img: "../../../../../../assets/Images/news-5.png",
      name: 'ĐẶT BAEMIN NHẬN NGHÌN KHUYẾN MÃI',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: '31.12.2020 - 05.01.2021',
      map: 'Tất cả cửa hàng Fruitee',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    },
    {
      id: 6,
      img: "../../../../../../assets/Images/news-6.png",
      name: 'MERRY CHRISTMAS - GIẢM 50% DÒNG SINH TỐ',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: '24.12.2020 - 25.12.2020',
      map: 'Tất cả cửa hàng Fruitee',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    },
    {
      id: 7,
      img: "../../../../../../assets/Images/news-7.png",
      name: 'UỐNG FRUITEE - QUẸT VÍ MOMO - 20%',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: '18.12.2020 - 25.12.2020',
      map: 'Chỉ áp dụng tại Fruitee Sư Vạn Hạnh',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    },
    {
      id: 8,
      img: "../../../../../../assets/Images/news-8.png",
      name: 'CÙNG FRUITEE BẢO VỆ MÔI TRƯỜNG',
      text: 'Còn gì tuyệt vời hơn việc ngồi phòng máy lạnh mát lành, thưởng thức ly trà sữa Fruitee vừa thơm ngon mà con được ưu đãi mua 1 tặng 1 phải không nào?',
      text1: ' Trà ngon giá ưu đãi và không gian xinh xắn để check-in đang chờ bạn tại Fruitee đấy, có lý do nào mà không dành chút thời gian ghé qua bạn nhỉ?',
      time: 'Không giới hạn',
      map: 'Tất cả cửa hàng Fruitee',
      discount: 'Tặng 1 ly trà sữa cùng loại khi mua bất kì trà sữa nào khi mua trực tiếp tại cửa hàng Fruitee.'
    }
  ];
  constructor(public dialog:MatDialog) { }

  ngOnInit(): void {
  }

  showOn(res){
    const dialogRef = this.dialog.open(DialogDetailComponent, {
      width: '1100px',
      data: res
    });
  }


}
